import java.util.Scanner;

public class NumberLine{
    // Problem 1) Number Line
    static Scanner si = new Scanner(System.in);
    
    public static void main( String[] args){
	
	int sampleInput = si.nextInt();
	
	for (int i = 1; i <= sampleInput; i++) {
	    System.out.print(i);
	}
	
	System.out.print("\n");
    }
}
