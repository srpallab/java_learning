import java.util.Scanner;

public class RectangleNumber{
    // Problem 3 Rectangle Line
    static Scanner si = new Scanner(System.in);
    
    public static void main( String[] args){
	//System.out.println("Sample Input: ");
	int sampleInputFirst = si.nextInt();
	int sampleInputSecond = si.nextInt();
	for (int i = 0; i < sampleInputFirst; i++) {
	    for (int j = 0; j < sampleInputSecond; j++) {
		System.out.print(j+1);	
	    }
	    System.out.print("\n");
	}
    }
}
