import java.util.Scanner;

public class Triangle{
    // Problem 7 
    static Scanner si = new Scanner(System.in);
    
    public static void main( String[] args){
	//System.out.println("Sample Input: ");
	int sampleInputFirst = si.nextInt();
	for (int i = 1; i <= sampleInputFirst; i++) {
	    for (int j = sampleInputFirst; j > i; j--) {
		System.out.print(" ");
	    }
	    for (int k = 0; k < i; k++) {
		System.out.print("*");
	    }
	    for (int m = 1; m < i; m++) {
		System.out.print("*");
	    }
	    System.out.print("\n");
	}
    }
}
