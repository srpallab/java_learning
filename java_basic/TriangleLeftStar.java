import java.util.Scanner;

public class TriangleLeftStar{
    // Problem 3 Rectangle Line
    static Scanner si = new Scanner(System.in);
    
    public static void main( String[] args){
	//System.out.println("Sample Input: ");
	int sampleInputFirst = si.nextInt();
	
	//int sampleInputSecond = si.nextInt();
	
	for (int i = 1; i < sampleInputFirst + 1 ; i++) {
	    for (int j = 0; j < i; j++) {
		System.out.print("*");	
	    }
	    System.out.print("\n");
	}
    }
}
