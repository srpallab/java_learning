import java.util.Scanner;

public class TriangleNumber{
    // Problem 7 
    static Scanner si = new Scanner(System.in);
    
    public static void main( String[] args){
	int count = 0;
	int sampleInputFirst = si.nextInt();
	for (int i = 1; i <= sampleInputFirst; i++) {
	    
	    for (int j = sampleInputFirst; j > i; j--) {
		System.out.print(" ");
	    }
	    for (int k = 0; k < i; k++) {
		System.out.print(k+1);
		count = k;
	    }
	    for (int m = 1; m < i; m++) {
		System.out.print(count+2);
		count = count + 1;
	    }
	    System.out.print("\n");
	}
    }
}
