import java.util.Scanner;

public class TriangleRightNumber{
    // Problem 7 
    static Scanner si = new Scanner(System.in);
    
    public static void main( String[] args){
	//System.out.println("Sample Input: ");
	int sampleInputFirst = si.nextInt();
	
	//int sampleInputSecond = si.nextInt();
	
	for (int i = 1; i <= sampleInputFirst; i++) {
	    for (int j = sampleInputFirst; j > i; j--) {
		System.out.print(" ");
	    }
	    for (int k = 0; k < i; k++) {
		System.out.print(k+1);
	    }
	    System.out.print("\n");
	}
    }
}
