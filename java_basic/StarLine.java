import java.util.Scanner;

public class StarLine{
    // Problem 2 Star Line
    static Scanner si = new Scanner(System.in);
    
    public static void main( String[] args){
	//System.out.println("Sample Input: ");
	int sampleInput = si.nextInt();
	for (int i = 1; i <= sampleInput; i++) {
	    System.out.print("*");
	}
	System.out.print("\n");
    }
}
